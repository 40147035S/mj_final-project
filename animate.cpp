#include "animate.h"
#include <stdio.h>
#include <stdlib.h>

bool timer::_second_passed (const float time)
{
    double duration;
    start = clock();

    while(1) {
        finish = clock();
        duration = (double)(finish - start) / CLOCKS_PER_SEC;
        if(duration >= time) break;
    }
    return true;
}
