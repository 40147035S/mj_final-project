varying vec3 N;
varying vec3 v;
varying vec2 texcoord;

void main()
{
	v = vec3(gl_ModelViewMatrix * gl_Vertex);
	N = normalize( gl_NormalMatrix*gl_Normal);
	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
	
	texcoord = gl_MultiTexCoord0.st;
	//gl_Position = ftransform();
}