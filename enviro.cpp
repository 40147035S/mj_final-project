#include "enviro.h"

lighting::lighting(float x1,float y1,float z1)
:light_pos(x1,y1,z1)
{
    light_theta = 0.0;
    spotlight = false;
}


void lighting::open_the_light0()
{
    GLfloat light0_[] = {0.0, 1.0, 0.0, 1.0};

    GLfloat diffuse0[] = {0.2,0.1,0.9,1.0};
    GLfloat ambient0[] = {1.0,1.0,1.0,1.0};
    GLfloat specular0[] = {0.0,1.0,1.0,1.0};

    light0_[0] = light_pos.x_();
    light0_[1] = light_pos.y_();
    light0_[2] = light_pos.z_();

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glLightfv(GL_LIGHT0, GL_POSITION, light0_ );
    glLightfv(GL_LIGHT0, GL_DIFFUSE, diffuse0 );
    glLightfv(GL_LIGHT0, GL_AMBIENT , ambient0);
    glLightfv(GL_LIGHT0, GL_SPECULAR, specular0);
}

void lighting::open_the_light1()
{
    GLfloat light1_[] = {0.0, 1.0, 0.0, 1.0};

    GLfloat diffuse1[] = {0.2,0.96,0.0625,1.0};
    GLfloat ambient1[] = {1.0,1.0,1.0,1.0};
    GLfloat specular1[] = {0.0,1.0,1.0,1.0};

    light1_[0] = light_pos.x_();
    light1_[1] = light_pos.y_();
    light1_[2] = light_pos.z_();

    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT1);
    glLightfv(GL_LIGHT1, GL_POSITION, light1_);
    glLightfv(GL_LIGHT1, GL_DIFFUSE, diffuse1);
    glLightfv(GL_LIGHT1, GL_AMBIENT, ambient1);
    glLightfv(GL_LIGHT1, GL_SPECULAR, specular1);
}


void lighting::theta_plus()
{
    light_theta+=Pi/12;
    if(light_theta>=Pi*2)
    {
        light_theta-=Pi*2;
    }
    light_pos.new_x( sin(light_theta)*2 );
    light_pos.new_y( cos(light_theta)*2 );
}
void lighting::theta_minus()
{
    light_theta-=Pi/12;
    if(light_theta<=0)
    {
        light_theta+=Pi*2;
    }
    light_pos.new_x( sin(light_theta)*2 );
    light_pos.new_y( cos(light_theta)*2 );
}
