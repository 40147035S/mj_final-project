#include "OBJ_loader.h"

using namespace std;

OBJ::OBJ (float X1,float Y1,float Z1,float roty,float rotz)
:P1(X1,Y1,Z1),ROT_y(roty),ROT_z(rotz),myPose(0)
{}
void OBJ::SetPosition( float x1,float y1,float z1 )
{
    P1.new_pos(x1,y1,z1);
}

void OBJ::drawOBJ()
{
 if (!myObj) return;

    for (GLMgroup *groups = myObj->groups; groups != NULL; groups = groups->next) {
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, myObj->materials[groups->material].textureID);
        for(unsigned i=0; i<groups->numtriangles; i+=1) {
            glBegin(GL_TRIANGLES);
                for (int j=0; j<3; j+=1)
                {
                    glNormal3fv(&myObj->normals[myObj->triangles[groups->triangles[i]].nindices[j]*3]);
                    glTexCoord2fv(&myObj->texcoords[myObj->triangles[groups->triangles[i]].tindices[j]*2]);
                    glVertex3fv(&myObj->vertices[myObj->triangles[groups->triangles[i]].vindices[j]*3]);
                }
            glEnd();
        }
    }
}

void OBJ::Translate()
{
    if(!move_horizon) {
        glTranslatef( P1.x_() + Pace, P1.y_(), P1.z_() );
    }
    else {
        glTranslatef( P1.x_() , P1.y_(), P1.z_() - Pace );
    }
}

void OBJ::Re_Translate()
{
    if(!move_horizon) {
        glTranslatef( ( P1.x_() + Pace ) * (-1), P1.y_() * (-1), P1.z_() * (-1) );
    }
    else {
        glTranslatef( P1.x_() * (-1) , P1.y_() * (-1), ( P1.z_() - Pace ) * (-1) );
    }
}

void OBJ::RotateY()
{
    glRotatef(ROT_y, 0.0, 1.0, 0.0);
}

void OBJ::Re_RotateY()
{
    glRotatef(ROT_y * (-1), 0.0, 1.0, 0.0);
}

void OBJ::RotateZ()
{
    glRotatef(ROT_z, 1.0, 0.0, 0.0);
}

void OBJ::Re_RotateZ()
{
    glRotatef(ROT_z * (-1), 1.0, 0.0, 0.0);
}

void OBJ::nextPose() {
    myPose++;
    if(myPose >= 29 ) myPose%=29;
}

void OBJ::mov_back() {
    if(!move_horizon) {
        P1.x_Amov( Pace );
    }
    else {
        P1.z_Mmov( Pace );
    }

}

void OBJ::hit_boundary(float lhs,float rhs)
{
    if(!move_horizon) {
        if(P1.x_() < lhs || P1.x_() > rhs ) {
            reverse_pace();
            more_degY( 180 );
        }
    }
    else {
        if(P1.z_() < lhs || P1.z_() > rhs ) {
            reverse_pace();
            more_degY( 180 );
        }
    }
}

OBJ_Group::OBJ_Group(size_t num,size_t pose) {
    Group.clear();

    for(size_t i=0;i<num;i++) {
        OBJ tmp(pose);
        Group.push_back(tmp);
    }
}

void OBJ_Group::drawOBJs()
{
    for( size_t i=0; i<size_(); i++ ) {
        Group[i].drawOBJ();
    } return;
}

void OBJ_Group::Unitize()
{
    for( size_t i=0; i<size_(); i++ ) {
        Group[i].Unitize();
    } return;
}


void OBJ_Group::Set_the_World_UP()
{
    char FileName_[30][25] = { "wooddoll_00.obj", "wooddoll_01.obj", "wooddoll_02.obj", "wooddoll_03.obj", "wooddoll_04.obj",
                               "wooddoll_05.obj", "wooddoll_06.obj", "wooddoll_07.obj", "wooddoll_08.obj", "wooddoll_09.obj",
                               "wooddoll_10.obj", "wooddoll_11.obj", "wooddoll_12.obj", "wooddoll_13.obj", "wooddoll_14.obj",
                               "wooddoll_15.obj", "wooddoll_16.obj", "wooddoll_17.obj", "wooddoll_18.obj", "wooddoll_19.obj",
                               "wooddoll_20.obj", "wooddoll_21.obj", "wooddoll_22.obj", "wooddoll_23.obj", "wooddoll_24.obj",
                               "wooddoll_25.obj", "wooddoll_26.obj", "wooddoll_27.obj", "wooddoll_28.obj"};
    for( size_t i=0,L1=size_(); i<L1; i++ ) {
        size_t a1 = Group[i].showPose_();

        Group[i].Read( FileName_[a1] );
        Group[i].Unitize();

        Group[i].set_jump( 0.2 );
    }
}
void OBJ_Group::Stand_On_Stage()
{
    for( size_t i=0,L1=size_(); i<L1; i++ ) {
        switch(i) {
        case 0: {
            Group[i].SetPosition(7.0,5.00,3.5);
            Group[i].set_degY( 90.0 );
            Group[i].set_horizon( false );
            Group[i].set_pace( -0.13 );
            Group[i].set_degZ( 30.0 );
        }break;
        case 1: {
            Group[i].SetPosition(0.0,5.00,-3.5);
            Group[i].set_degY( 270.0 );
            Group[i].set_horizon( false );
            Group[i].set_pace( 0.13 );
            Group[i].set_degZ( 30.0 );
        }break;
        case 2: {
            Group[i].SetPosition(7.0,5.00,-3.5);
            Group[i].set_degY( 180.0 );
            Group[i].set_horizon( true );
            Group[i].set_pace( -0.13 );
            Group[i].set_degZ( 30.0 );
        }break;
        case 3: {
            Group[i].SetPosition(0.0,5.00,3.5);
            Group[i].set_degY( 0.0 );
            Group[i].set_horizon( true );
            Group[i].set_pace( 0.13 );
            Group[i].set_degZ( 30.0 );
        }break;
        }
    }
}
void OBJ_Group::Rehersal()
{
    for( size_t i=0,L1=size_(); i<L1; i++ ) {
        Group[i].Translate();
        Group[i].RotateY();
        Group[i].RotateZ();
        Group[i].drawOBJ();
        Group[i].Re_RotateZ();
        Group[i].Re_RotateY();
        Group[i].Re_Translate();
    }
}

void OBJ_Group::GO_BackWard()
{
    for( size_t i=0,L1=size_(); i<L1; i++ ) {
        Group[i].mov_back();
    }
}

void OBJ_Group::next_pose()
{
    for( size_t i=0,L1=size_(); i<L1; i++ ) {
        Group[i].nextPose();
    }
}

void OBJ_Group::set_degZ(float deg)
{
    for( size_t i=0,L1=size_(); i<L1; i++ ) {
        Group[i].set_degZ( deg );
    }
}

void OBJ_Group::check_boundary()
{
    for( size_t i=0,L1=size_(); i<L1; i++ ) {
        switch( i )    {
        case 0: case 1:{
                Group[i].hit_boundary(0.0 , 7.0);
            }break;
        case 2: case 3: {
                Group[i].hit_boundary(-3.5 , 3.5);
            }break;
        }
    }
}
