#ifndef OBJ_LOADER_H_INCLUDED
#define OBJ_LOADER_H_INCLUDED

#include <windows.h>
#define GLEW_STATIC

#include <vector>
#include "position.h"


class OBJ
{
public:

    OBJ () :myPose(0) {}
    OBJ (const size_t pos_) :myPose(pos_) { }
    OBJ (float X1,float Y1,float Z1,float roty,float rotz);
    OBJ (const OBJ& tmp) :P1(tmp.P1),myObj(tmp.myObj), myPose(tmp.myPose) {}


    void Read(char filename[]) { myObj = glmReadOBJ(filename); }

    void Unitize() { glmUnitize(myObj); }
    void SetPosition( float x1,float y1,float z1 );

    void drawOBJ();

    void Translate();
    void Re_Translate();

    void RotateY();
    void Re_RotateY();
    void RotateZ();
    void Re_RotateZ();

    void more_degY (float d1) {
        ROT_y += d1, ROT_y = (ROT_y>=360)?ROT_y-360 : ROT_y; }
    void less_degY (float d2) {
        ROT_y -= d2, ROT_y = (ROT_y<= 0 )?ROT_y+360 : ROT_y; }
    void set_degY (float d3) { ROT_y = d3; }

    void more_degZ (float d1) { ROT_z += d1; }
    void less_degZ (float d2) { ROT_z -= d2; }
    void set_degZ (float d3) { ROT_z = d3; }

    void set_pace(float pac_) { Pace = pac_; }
    void add_pace(float pac_) { Pace += pac_; }
    void minus_pace(float pac_) { Pace -= pac_; }
    void reverse_pace () { Pace*= (-1); }

    void set_jump(float jum_) { Jump = jum_; }
    void add_jump(float jum_) { Jump += jum_; }
    void minus_jump(float jum_) { Jump -= jum_; }
    void reverse_jump() { Jump*= (-1); }

    size_t showPose_() const { return myPose; }
    void nextPose();
    void GoToPose(size_t P_) { myPose = P_; }

    void mov_back();
    void set_horizon(bool B) { move_horizon = B; }
    void hit_boundary(float lhs,float rhs);

    position my_place () const { return P1; }
private:
    position P1;
    float Bend;
    float ROT_y,ROT_z,Pace,Jump;
    GLMmodel *myObj ;
    bool move_horizon;

    size_t myPose;
};

class OBJ_Group
{
public:
    OBJ_Group() { Group.clear(); }
    OBJ_Group(size_t num,size_t pose);

    void Read(char filename[][20],size_t num);
    void drawOBJs();
    void Unitize();
    size_t size_() const { return Group.size(); }

    void Set_the_World_UP();
    void Stand_On_Stage();
    void Rehersal();
    void Run_the_Whole_World();
    void GO_BackWard();
    void next_pose();
    void set_degZ(float deg);
    void check_boundary();

    std::vector<OBJ> Group;
private:
};

#endif // OBJ_LOADER_H_INCLUDED
