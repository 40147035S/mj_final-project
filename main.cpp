//The headers
#include <Windows.h>    // for solving the Code::Blocks errors
#define GLEW_STATIC
#include <mmsystem.h>

#include "watcher.h"
#include "textfile.h"
#include "OBJ_loader.h"
#include "enviro.h"
#include "animate.h"

#pragma comment(lib, "winmm.lib")

using namespace std;

//Watcher W1(9.61, 7.91, -3.65, 147.0, -31.65, 0.6 );
Watcher W1(2.36, 2.29, -12.27, 86.0, 1.0, 0.6 );
lighting L1(-2.17,9.38,-1.23);
lighting S1(7.01, 8.04, 2.91),S2(-0.75, 7.48, -2.16);
Mouse M1;

//----functions
void display(void);
void animate();
void setShaders();
void myReshape(int w, int h);

void mouse(int btn, int state, int x, int y);
void motion(int x, int y);
void keyboard(unsigned char key, int x, int y);
void SpecialKeyBoard(int key, int x, int y);

//----functions

char background[25] = { "pontez.obj" };
OBJ_Group ActorS(4,0);
timer T1;
OBJ WDD,WD2;

bool animated = false; //show start!

int main(int argc, char **argv)
{
    PlaySound((LPCSTR) "back.wav", NULL, SND_FILENAME | SND_ASYNC);

    glutInit(&argc, argv);

    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH);
    glutInitWindowSize(650,650);
    glutCreateWindow("Dancing Club _Final Project");
    glutReshapeFunc(myReshape);
    glutIdleFunc(animate);
    glutDisplayFunc(display);

    glutKeyboardFunc(keyboard);
    glutMouseFunc(mouse);
    glutSpecialFunc(SpecialKeyBoard);
    glutMotionFunc(motion);

    glEnable(GL_TEXTURE_2D);
    glEnable(GL_DEPTH_TEST); /* Enable hidden--surface--removal */

    glClearColor(0.0f,0.0f,0.3f,1.0f);
    glewInit();

    WD2.Read(background); //"the stage"
    ActorS.Stand_On_Stage();


    setShaders();
    glutMainLoop();

    return 0;
}

int a = 0;
float Feet = 0.4,jump=0.2 , Rot = 5.0;

void display(void)
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glLoadIdentity();

//lights----------------------------------------
     L1.open_the_light0();  //the ambient light
//lights----------------------------------------

//the watcher-----------------------------------
    W1.LookAT();
    W1.print();
//the watcher-----------------------------------

//Actors on Stage===============================
    ActorS.Set_the_World_UP();
    WD2.drawOBJ();
    ActorS.Rehersal();
//Actors on Stage===============================

    glFlush();
    glutSwapBuffers();
}

void animate()
{
    if(animated) {
        S1.open_the_light1();

        if(T1._second_passed(0.01)) {
            ActorS.check_boundary();
            ActorS.GO_BackWard();
            ActorS.next_pose();
            ActorS.set_degZ(45.0);
        }
        glutPostRedisplay();
    }
}

void setShaders() {

    GLhandleARB v,f,p;
    //GLhandleARB f2;
	static int inited = 0;
	char *vs = NULL,*fs = NULL;

	if (! inited) {
		v = glCreateShaderObjectARB(GL_VERTEX_SHADER_ARB);
		f = glCreateShaderObjectARB(GL_FRAGMENT_SHADER_ARB);
	}

    vs = textFileRead("F.frag");
    fs = textFileRead("V.vert");

	const char * vv = vs;
	const char * ff = fs;

	glShaderSourceARB(v, 1, &vv,NULL);
	glShaderSourceARB(f, 1, &ff,NULL);

	free(vs);free(fs);

	glCompileShaderARB(v);
	glCompileShaderARB(f);

	if (! inited) {
		p = glCreateProgramObjectARB();
	}
	glAttachObjectARB(p,v);

	glLinkProgramARB(p);
    glUseProgramObjectARB(p);
	glAttachObjectARB(p,f);

	glLinkProgramARB(p);
    glUseProgramObjectARB(p);
}

void keyboard(unsigned char key, int x, int y)
{
    //te exit the program
    if(key == 'Q' || key == 'q') exit(0);

    if(key == 'T' || key == 't') {
        animated = !animated;
        if(animated){
            PlaySound((LPCSTR) "123.wav", NULL, SND_FILENAME | SND_ASYNC);
        }
    }

    //轉身
    if(key == 'A' || key == 'a')
    {
        W1.Minus_degree(2.5);
    }
    if(key == 'D' || key == 'd')
    {
        W1.Add_degree(2.5);
    }

    //俯視/仰望
    if(key == 'W' || key == 'w')
    {
        W1.Add_upper(1);
    }
    if(key == ' ' || key == ' ')
    {

        W1.Back_to_Horizon();
    }
    if(key == 'S' || key == 's')
    {
        W1.Minus_upper(1);
    }

    //視線向上平移/向下平移
    if(key == 'Z' || key == 'z')
    {
        W1.Add_raise(0.1);
    }
    if(key == 'X' || key == 'x')
    {
        W1.Minus_raise(0.1);
    }

    if(key=='+')
    {
        L1.theta_plus();
    }
    else if(key=='-')
    {
        L1.theta_minus();
    }

    glutPostRedisplay();

}

void motion(int x, int y)
{
    if(M1.is_LeftBut_Clic() && M1.M_State() == GLUT_DOWN )
    {
        if(M1.M_Button()==GLUT_LEFT_BUTTON)
        {
            int M = ( M1._MouseX() - x )/2;
            (M>0)? W1.Add_degree(M) : W1.Minus_degree(M*-1);

            int U = ( M1._MouseY() - y )/2;
            (U>0)? W1.Add_upper(U) : W1.Minus_upper(U*-1);
        }
    }
    M1.New_Position(x,y);

    glutPostRedisplay();
}

void mouse(int btn, int state, int x, int y)
{
    if(btn==GLUT_LEFT_BUTTON && state == GLUT_DOWN)
    {
        M1.left_button(1);
        M1.MState_alter(state);
        M1.MButton_alter(btn);
        M1.New_Position(x,y);
    }
    else
    {
        M1.left_button(0);
    }
}

void SpecialKeyBoard(int key, int x, int y)
{
    //前後左右
    if(key == GLUT_KEY_UP)
    {
        W1.Move_Forward(0.2);
    }

    if(key == GLUT_KEY_DOWN)
    {
        W1.Move_Backward(0.2);
    }

    if(key == GLUT_KEY_LEFT)
    {
        W1.Move_Left(0.2);
    }

    if(key == GLUT_KEY_RIGHT)
    {
        W1.Move_Right(0.2);
    }
    glutPostRedisplay();
}

//----------------------------
void myReshape(int w, int h)
{
    glViewport(0, 0, w, h);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(65.0, 1.0, 0.1, 12.0);
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}


