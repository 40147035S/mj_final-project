#ifndef ANIMATE_H_INCLUDED
#define ANIMATE_H_INCLUDED

#include <time.h>

class timer
{
public:
    bool _second_passed (const float time);
private:
    clock_t start, finish;
};

class motion
{
public:
private:
};
#endif // ANIMATE_H_INCLUDED
